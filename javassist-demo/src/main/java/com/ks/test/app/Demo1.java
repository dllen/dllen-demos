package com.ks.test.app;

import javassist.ClassPool;
import javassist.CtClass;

public class Demo1 {

    public static void main(String[] args) throws Exception {
        ClassPool pool = ClassPool.getDefault();

        CtClass cc = pool.get("com.ks.test.Rectangle");
        cc.setSuperclass(pool.get("com.ks.test.Point"));
        // obtain the bytecode
        // byte[] b = cc.toBytecode();
        // load the class
        // Class clazz = cc.toClass();
        cc.writeFile(".");
    }
    /*
    在项目根目录执行：javap -c com.ks.test.Rectangle
    Compiled from "Rectangle.java"
    public class com.ks.test.Rectangle extends com.ks.test.Point {
      public com.ks.test.Rectangle();
        Code:
           0: aload_0
           1: invokespecial #18                 // Method com/ks/test/Point."<init>":()V
           4: return
    }
     */


    // javap 使用方式
    /*
    Usage: javap <options> <classes>...

    where options include:
       -c                        Disassemble the code
       -classpath <pathlist>     Specify where to find user class files
       -extdirs <dirs>           Override location of installed extensions
       -help                     Print this usage message
       -J<flag>                  Pass <flag> directly to the runtime system
       -l                        Print line number and local variable tables
       -public                   Show only public classes and members
       -protected                Show protected/public classes and members
       -package                  Show package/protected/public classes
                                 and members (default)
       -private                  Show all classes and members
       -s                        Print internal type signatures
       -bootclasspath <pathlist> Override location of class files loaded
                                 by the bootstrap class loader
       -verbose                  Print stack size, number of locals and args for methods
                                 If verifying, print reasons for failure
     */
}
