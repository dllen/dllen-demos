package com.ks.test.app;

import javassist.ClassPool;
import javassist.CtClass;

public class Demo2 {

    public static void main(String[] args) throws Exception{

        ClassPool classPool = ClassPool.getDefault();
        CtClass cc = classPool.makeClass("com.ks.test.SimplePoint");
        // addMethod()
        // addField()
        // addConstructor()
        // addInterface()
        // setSuperclass()

        cc.writeFile(".");
    }

    /*
    javap -c com.ks.test.SimplePoint

    Compiled from "SimplePoint.java"
    public class com.ks.test.SimplePoint {
      public com.ks.test.SimplePoint();
        Code:
           0: aload_0
           1: invokespecial #10                 // Method java/lang/Object."<init>":()V
           4: return
    }
     */
}
