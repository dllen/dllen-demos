package com.ks.test.app;

import javassist.ClassPool;
import javassist.CtClass;

public class Demo4 {

    public static void main(String[] args) throws Exception{

        ClassPool classPool = ClassPool.getDefault();

        CtClass ctClass = classPool.get("com.ks.test.Rectangle");

        ctClass.prune();
        // The pruned CtClass object cannot be defrost again. The default value of ClassPool.doPruning is false.
        // ctClass.defrost();

        ctClass.stopPruning(true);
        // is not pruned
        // The CtClass object cc is not pruned. Thus it can be defrost after writeFile() is called.
        ctClass.writeFile();

    }
}
