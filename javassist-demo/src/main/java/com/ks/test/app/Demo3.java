package com.ks.test.app;

import javassist.ClassPool;
import javassist.CtClass;

public class Demo3 {


    public static void main(String[] args) throws Exception{
        // If a CtClass object is converted into a class file by writeFile(), toClass(), or toBytecode(), Javassist freezes that CtClass object.
        ClassPool classPool = ClassPool.getDefault();
        CtClass cc = classPool.get("com.ks.test.Rectangle");
        cc.writeFile();
        cc.defrost();
        // OK since the class is not frozen.
        // After defrost() is called, the CtClass object can be modified again.
        cc.setSuperclass(classPool.get("com.ks.test.Point"));
    }
}
