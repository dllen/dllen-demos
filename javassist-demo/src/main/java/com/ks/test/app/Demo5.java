package com.ks.test.app;

import javassist.ClassClassPath;
import javassist.ClassPath;
import javassist.ClassPool;
import javassist.URLClassPath;

public class Demo5 {

    public static void main(String[] args) throws Exception {

        ClassPool classPool = ClassPool.getDefault();

        // init classpath with current directory
        classPool.insertClassPath(new ClassClassPath(Demo5.class));

        // add local directory to classpath
        classPool.insertClassPath("/usr/local/javalib");

        // load class from javassist.org
        ClassPath cp = new URLClassPath("www.javassist.org", 80, "/java/", "org.javassist.");
        classPool.insertClassPath(cp);

        // load class from byte array
        /*
            ClassPool cp = ClassPool.getDefault();
            byte[] b = a byte array;
            String name = class name;
            cp.insertClassPath(new ByteArrayClassPath(name, b));
            CtClass cc = cp.get(name);
         */

        // load class from input stream
        /*
            ClassPool cp = ClassPool.getDefault();
            InputStream ins = an input stream for reading a class file;
            CtClass cc = cp.makeClass(ins);

         */
    }

}
