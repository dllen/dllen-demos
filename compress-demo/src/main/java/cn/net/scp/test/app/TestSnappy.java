package cn.net.scp.test.app;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.xerial.snappy.Snappy;

public class TestSnappy {
    public static byte[] compress(byte srcBytes[]) throws IOException {
        return  Snappy.compress(srcBytes);
    }
    
    public static byte[] uncompress(byte[] bytes) throws IOException {
        return Snappy.uncompress(bytes);
    }

    public static void main(String[] args) throws Exception {
        String str = "189988hdeaadeqq";
        byte[] bytes = str.getBytes(StandardCharsets.UTF_8);

        byte[] compressBytes = compress(bytes);
        byte[] uncompressBytes = uncompress(compressBytes);
        System.out.println(new String(uncompressBytes, StandardCharsets.UTF_8));
    }
}
