package cn.net.scp.test.app;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;

public class TestBzip2 {

    public static byte[] compress(byte[] input) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        BZip2CompressorOutputStream bZip2CompressorOutputStream = new BZip2CompressorOutputStream(
                byteArrayOutputStream);

        bZip2CompressorOutputStream.write(input);
        bZip2CompressorOutputStream.close();

        return byteArrayOutputStream.toByteArray();
    }

    public static byte[] uncompress(byte[] input) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(input);


        try {
            BZip2CompressorInputStream bZip2CompressorInputStream = new BZip2CompressorInputStream(byteArrayInputStream);
            byte[] buffer = new byte[1024];
            int n;
            while ((n = bZip2CompressorInputStream.read(buffer)) > 0) {
                byteArrayOutputStream.write(buffer, 0, n);
            }
            bZip2CompressorInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return byteArrayOutputStream.toByteArray();
    }


    public static void main(String[] args) throws Exception {
        String str = "189988hdeaadeqq";
        byte[] bytes = str.getBytes(StandardCharsets.UTF_8);

        byte[] compressBytes = compress(bytes);
        byte[] uncompressBytes = uncompress(compressBytes);
        System.out.println(new String(uncompressBytes, StandardCharsets.UTF_8));
    }



}
