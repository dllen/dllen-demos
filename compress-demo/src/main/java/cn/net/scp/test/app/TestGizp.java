package cn.net.scp.test.app;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class TestGizp {

    public static byte[] compress(byte input[]) {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        GZIPOutputStream gzipOutputStream;

        try {
            gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gzipOutputStream.write(input);
            gzipOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream.toByteArray();
    }

    public static byte[] uncompress(byte[] input) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(input);
            GZIPInputStream gzipInputStream = new GZIPInputStream(byteArrayInputStream);
            byte[] buffer = new byte[1024];
            int n;

            while ((n = gzipInputStream.read(buffer)) > 0) {
                byteArrayOutputStream.write(buffer, 0, n);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream.toByteArray();
    }

    public static void main(String[] args) {
        String str = "189988hdeaadeqq";
        byte[] bytes = str.getBytes(StandardCharsets.UTF_8);

        byte[] compressBytes = compress(bytes);
        byte[] uncompressBytes = uncompress(compressBytes);
        System.out.println(new String(uncompressBytes, StandardCharsets.UTF_8));
    }

}
