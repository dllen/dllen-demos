package cn.net.scp.test.app;

import java.util.Arrays;

public class Lcr164 {

    public static void main(String[] args) {

        int[] arr1 = new int[]{0, 3, 30, 34, 5, 9};
        // 0330
        // 0303

//        Integer[] arr2 = new Integer[]{0, 3, 30, 34, 5, 9};
//
//        Arrays.sort(arr2, (a, b) -> (a + "" + b).compareTo(b + "" + a));
//
//        System.out.println(Arrays.toString(arr2));
//
//        String res = "";
//        for (Integer i : arr2) {
//            res += i;
//        }




        int[] tmpArr = arr1;
        int len = tmpArr.length;
        for (int i = 0; i < len; i++) {
            int a = tmpArr[i];
            for (int j = i + 1; j < len - 1; j++) {
                int b = tmpArr[j];
                String str1 = (a + "" + b);
                String str2 = (b + "" + a);
                if (str1.compareTo(str2) > 0) {
                   tmpArr[i] = b;
                   tmpArr[j] = a;
                }

            }
            if (i == len - 1) {
                int a1 = tmpArr[i - 1];
                int b1 = tmpArr[i];
                String str1 = a1 + "" + b1;
                String str2 = b1 + "" + a1;
                if (str1.compareTo(str2) > 0) {
                    tmpArr[i - 1] = b1;
                    tmpArr[i] = a1;
                }
            }
        }

        System.out.println(Arrays.toString(tmpArr));
    }

}
