package cn.net.scp.test.app;

import java.util.HashMap;
import java.util.Map;

public class LeetCode169 {

    public static void main(String[] args) {

    }

    public static int majorityElement(int[] nums) {
        if(nums.length==0){
            return 0;
        }
        Map<Integer, Integer> m = new HashMap<>(nums.length);
        for (int num : nums) {
            m.put(num, m.getOrDefault(num, 0) + 1);
            if (m.get(num) > nums.length / 2) {
                return num;
            }
        }

        return 0;
    }

}
