package cn.net.scp.test.app;

public class LeetCode0106 {

    public static void main(String[] args) {

        String str = "aabcccccaaa";

        System.out.println(compressString(str));

    }


    public static String compressString(String S) {

        if(S==null || S.length()==0) {
            return S;
        }

        char[] chars = S.toCharArray();

        char c = chars[0];
        int num = 0;
        String res = "";
        for (char tmpChar : chars) {
            if (tmpChar == c) {
                num++;
            } else {
                res += c + String.valueOf(num);
                c = tmpChar;
                num = 1;
            }
        }
        res += c + String.valueOf(num);
        if(res.length() > S.length()) {
            return S;
        }

        return res;
    }

}
