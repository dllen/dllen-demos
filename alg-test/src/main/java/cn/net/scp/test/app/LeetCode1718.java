package cn.net.scp.test.app;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LeetCode1718 {

    public static void main(String[] args) {

        int arr1[] = {7,5,9,0,2,1,3,5,7,9,1,1,5,8,8,9,7};
        int arr2[] = {1,5,9};

        System.out.println(Arrays.toString(shortestSeq(arr1, arr2)));

    }

    public static int[] shortestSeq(int[] big, int[] small) {
        Set<Integer> set = new HashSet<>();
        Map<Integer, Integer> map = new HashMap<>();
        for (int i : small) {
            set.add(i);
        }
        int[] res = new int[2];

        int left = 0;
        int min = Integer.MAX_VALUE;

        for (int i = 0; i < big.length; i++) {
            if (set.contains(big[i])) {
                map.put(big[i], i);
            }
            // 重新计算窗口
            while (set.size() == map.size()) {
                // 检查机制
                if (set.contains(big[left]) && left >= map.get(big[left])) {
                    if (min > i - left) {
                        min = i - left;
                        res[0] = left;
                        res[1] = i;
                    }
                    map.remove(big[left]);
                }
                left++;
            }
        }

        if(res[0]==0 && res[1]==0){
            return new int[0];
        }

        return res;
    }

}
