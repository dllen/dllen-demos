package cn.net.scp.test.alg;

import org.roaringbitmap.longlong.LongBitmapDataProvider;
import org.roaringbitmap.longlong.LongIterator;
import org.roaringbitmap.longlong.Roaring64Bitmap;
import org.roaringbitmap.longlong.Roaring64NavigableMap;

public class TestRoaringBitmap {

    public static void main(String[] args) {
        // first Roaring64NavigableMap
        LongBitmapDataProvider r = Roaring64NavigableMap.bitmapOf(1, 2, 100, 1000);
        r.addLong(1234);
        System.out.println(r.contains(1)); // true
        System.out.println(r.contains(3)); // false
        LongIterator longIterator = r.getLongIterator();
        while (longIterator.hasNext()) {
            System.out.println(longIterator.next());
        }

        // second Roaring64Bitmap
        Roaring64Bitmap bitmap1 = new Roaring64Bitmap();
        Roaring64Bitmap bitmap2 = new Roaring64Bitmap();
        int k = 1 << 16;
        long i = Long.MAX_VALUE / 2;
        long base = i;
        for (; i < base + 10000; ++i) {
            bitmap1.add(i * k);
            bitmap2.add(i * k);
        }
        bitmap1.and(bitmap2);
    }

}
