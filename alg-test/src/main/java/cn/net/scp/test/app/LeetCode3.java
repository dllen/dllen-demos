package cn.net.scp.test.app;

public class LeetCode3 {


    public static void main(String[] args) {

        String str = "abcabcbb";

        System.out.println(lengthOfLongestSubstring(str));

    }

    public static int lengthOfLongestSubstringV2(String s){
        int[] last = new int[128];

        for (int i = 0; i < 128; i++) {
            last[i] = -1;
        }

        //aaa

        int n = s.length();
        int start = 0;
        int res = 0;
        for(int i = 0; i < n; i++){
            int c = s.charAt(i);
            start = Math.max(start, last[c] + 1);
            res = Math.max(res, i - start + 1);
            last[c] = i;
        }

        return res;

    }

    public static int lengthOfLongestSubstring(String s) {

        int[] last = new int[128];
        for (int i = 0; i < 128; i++) {
            last[i] = -1;
        }

        int n = s.length();

        int res = 0;
        int start = 0;

        for (int i = 0; i < n; i++) {
            int index = s.charAt(i);
            start = Math.max(start, last[index] + 1);
            res = Math.max(res, i - start + 1);
            last[index] = i;
        }

        return res;

    }

}
