package cn.net.scp.test.alg;

import java.util.BitSet;
import java.util.function.Function;

public class BloomFilters {

    private final BitSet bitSet;
    private final int size;
    private final Function<String, Integer>[] hashFunctions;

    public BloomFilters(BitSet bitSet, int size, Function<String, Integer>[] hashFunctions) {
        this.bitSet = bitSet;
        this.size = size;
        this.hashFunctions = hashFunctions;
    }

    public void add(String item) {
        for (Function<String, Integer> hashFunction : hashFunctions) {
            bitSet.set(hashFunction.apply(item) % size, true);
        }
    }

    public boolean mightContain(String item) {
        for (Function<String, Integer> hashFunction : hashFunctions) {
            if (!bitSet.get(hashFunction.apply(item) % size)) {
                return false;
            }
        }
        return true;
    }
}
