package cn.net.scp.test.app;

public class LeetCode206 {


    public static void main(String[] args) {

    }

    public static ListNode reverseList(ListNode head) {

        if (head == null) {
            return null;
        }

        ListNode curr = head.next;
        ListNode pre = head;
        pre.next = null;

        ListNode res = head;
        // pre -> curr
        // curr -> pre
        while (curr != null) {
            ListNode t = curr;
            t.next = pre;
            pre = t;
            res = t;

            curr = curr.next;
        }

        return res;
    }


    public class ListNode {

        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

}
