package cn.net.scp.test.alg;

public class Bitmap {

    private final long[] bits; // Array to hold the bits
    private final int size;    // Size of the bitmap

    public Bitmap(int size) {
        this.size = size;
        bits = new long[(size + 63) / 64]; // Each long can hold 64 bits
    }

    // Set the bit at index
    public void add(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        bits[index / 64] |= (1L << (index % 64)); // Set the bit
    }

    // Check if the bit at index is set
    public boolean contains(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        return (bits[index / 64] & (1L << (index % 64))) != 0; // Check the bit
    }

    // Remove the bit at index
    public void remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        bits[index / 64] &= ~(1L << (index % 64)); // Clear the bit
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(contains(i) ? "1" : "0");
            if ((i + 1) % 64 == 0) {
                sb.append("\n"); // New line every 64 bits
            }
        }
        return sb.toString();
    }
}
