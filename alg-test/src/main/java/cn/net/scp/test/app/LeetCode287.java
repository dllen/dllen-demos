package cn.net.scp.test.app;

import java.util.HashMap;
import java.util.Map;

public class LeetCode287 {


    public static void main(String[] args) {

        int arr[] = {1,3,4,2,2};

        System.out.println(findDuplicate(arr));

    }

    public static int findDuplicate(int[] nums) {

        Map<Integer, Integer> map = new HashMap<>();

        for (int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
            if(map.get(num) > 1){
                return num;
            }
        }
        return 0;
    }

}
