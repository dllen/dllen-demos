package cn.net.scp.test.app;

public class LeetCode165 {


    public static void main(String[] args) {
        String version1 = "1.01";
        String version2 = "1.001";
        System.out.println(new LeetCode165().compareVersion(version1, version2));
    }

    public int compareVersion(String version1, String version2) {

        String[] v1 = version1.split("\\.");
        String[] v2 = version2.split("\\.");
        int i = 0;
        while (i < v1.length || i < v2.length) {
            int v1i = i < v1.length ? Integer.parseInt(v1[i]) : 0;
            int v2i = i < v2.length ? Integer.parseInt(v2[i]) : 0;
            if (v1i < v2i) {
                return -1;
            } else if (v1i > v2i) {
                return 1;
            }
            i++;
        }
        return 0;
    }
}
