package cn.net.scp.test.app;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

public class TestForkJoin {

    private static ExecutorService executorService = Executors.newWorkStealingPool(10);

    private static class SomeCallable implements Callable<String> {

        private int counter;         //recursive counter
        private int childrenCount = 80;//amount of children to spawn
        private int idx;

        public SomeCallable(int counter, int idx) {
            this.counter = counter;
            this.idx = idx;
        }

        // just for displaying

        @Override
        public String call() throws Exception {
            Thread.sleep(5000L);
            ForkJoinPool pool = (ForkJoinPool) executorService;
            System.out.println(
                "counter=" + counter + "." + idx + " activeThreads=" + pool.getActiveThreadCount() + " runningThreads=" + pool.getRunningThreadCount() + " poolSize=" + pool.getPoolSize()
                    + " queuedTasks=" + pool.getQueuedTaskCount() + " queuedSubmissions=" + pool.getQueuedSubmissionCount() + " parallelism=" + pool.getParallelism() + " stealCount="
                    + pool.getStealCount() + " activeThreadCnt=" + Thread.activeCount());
            return null;
        }
    }


    private static class SomeAction extends RecursiveAction {

        private int counter;         //recursive counter
        private int childrenCount = 80;//amount of children to spawn
        private int idx;             // just for displaying

        private SomeAction(int counter, int idx) {
            this.counter = counter;
            this.idx = idx;
        }

        @Override
        protected void compute() {
            ForkJoinPool pool = (ForkJoinPool) executorService;
            System.out.println(
                "counter=" + counter + "." + idx + " activeThreads=" + pool.getActiveThreadCount() + " runningThreads=" + pool.getRunningThreadCount() + " poolSize=" + pool.getPoolSize()
                    + " queuedTasks=" + pool.getQueuedTaskCount() + " queuedSubmissions=" + pool.getQueuedSubmissionCount() + " parallelism=" + pool.getParallelism() + " stealCount="
                    + pool.getStealCount());
            if (counter <= 0) {
                return;
            }

            List<SomeAction> list = new ArrayList<>(childrenCount);
            for (int i = 0; i < childrenCount; i++) {
                SomeAction next = new SomeAction(counter - 1, i);
                list.add(next);
                next.fork();
            }

            for (SomeAction action : list) {
                action.join();
            }
        }
    }

    public static void main(String[] args) throws Exception {

        List<SomeCallable> callables = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            callables.add(new SomeCallable(i, 0));
        }

        executorService.invokeAll(callables);

    }
}
