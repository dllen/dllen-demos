package cn.net.scp.test.app;

import java.util.HashMap;
import java.util.Map;

public class LeetCode160 {


    public static void main(String[] args) {

    }


    public static ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if (headA == null || headB == null) {
            return null;
        }

        ListNode h1 = headA;
        ListNode h2 = headB;
        Map<Integer, Integer> mark = new HashMap<>();

        while (h1 != null) {
            mark.put(h1.hashCode(), 1);
            h1 = h1.next;
        }

        while (h2 != null) {
            int markVal = mark.getOrDefault(h2.hashCode(), 0);
            if (markVal > 0) {
                return h2;
            }
            h2 = h2.next;
        }

        return null;
    }

    class ListNode {

        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
            next = null;
        }
    }
}



