package cn.net.scp.test.alg;

public class TestBitmap {

    public static void main(String[] args) {
        Bitmap bitmap = new Bitmap(128); // Create a bitmap for 128 bits

        bitmap.add(2);
        bitmap.add(3);
        bitmap.add(64);

        System.out.println("Bitmap contains:");
        System.out.println(bitmap); // Display the current state of the bitmap

        System.out.println("Contains 2: " + bitmap.contains(2)); // true
        System.out.println("Contains 5: " + bitmap.contains(5)); // false

        bitmap.remove(2);
        System.out.println("After removing index 2:");
        System.out.println(bitmap); // Display updated state
    }
}
