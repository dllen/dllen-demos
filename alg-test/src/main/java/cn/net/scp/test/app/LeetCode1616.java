package cn.net.scp.test.app;

import java.util.Arrays;

public class LeetCode1616 {


    public static void main(String[] args) {
        int[] arr = new int[]{1,2,4,7,10,11,7,12,6,7,16,18,19};
        System.out.println(Arrays.toString(subSort(arr)));
    }


    public static int[] subSort(int[] array) {

        int N = array.length;
        int start = -1;
        int end = -1;

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < N; i++) {
            if (array[i] >= max) {
                max = array[i];
            } else {
                end = i;
            }
        }
        if (end == -1) {
            return new int[]{-1, -1};
        }

        for (int i = end; i >= 0; i--) {
            if (array[i] <= min) {
                min = array[i];
            } else {
                start = i;
            }
        }

        return new int[]{start, end};

    }
}
