package cn.net.scp.test.app;

public class LeetCode11 {

    public static void main(String[] args) {

        int[] arr = new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7};

        int maxVal = 0;
        for (int i = 0; i < arr.length; i++) {
            int h1 = arr[i];
            for (int j = i + 1; j < arr.length; j++) {
                int h2 = arr[j];
                int h = Math.min(h1, h2);
                int w = j - i;
                int val = h * w;
                if (val > maxVal) {
                    maxVal = val;
                }
            }
        }

        System.out.println("maxVal: " + maxVal);

        int left = 0, right = arr.length - 1;
        int maxArea = 0;

        while (left < right) {
            if (arr[left] < arr[right]) {
                maxArea = Math.max(maxArea, arr[left] * (right - left));
                left++;
            } else {
                maxArea = Math.max(maxArea, arr[right] * (right - left));
                right--;
            }
        }

        System.out.println("maxArea: " + maxArea);

    }

}
