package cn.net.scp.test.app;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestForkJoin2 {

    public static void main(String[] args) throws InterruptedException {
        Collection<Callable<Void>> tasks = new ArrayList<>();
        ExecutorService executorService = Executors.newWorkStealingPool(10);

        for (int i = 0; i < 20; i++) {
            int taskNumber = i;
            Callable<Void> callable = () -> {
                Thread.sleep(1000);
                System.out.println("Processed user request #" + taskNumber + " on thread " + Thread.currentThread().getName());
                return null;
            };
            tasks.add(callable);
        }
        executorService.invokeAll(tasks);
    }

}
