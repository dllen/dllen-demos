package cn.net.scp.test.app;

public class LeetCode97 {

    public static void main(String[] args) {

        String str1 = "aabcc";
        String str2 = "dbbca";
        String str3 = "aadbbcbcac";


    }




    public boolean isInterleave(String s1, String s2, String s3) {
        // s1的前i位和s2的前j位是否能组成s3的前i + j位
        // if(s1.charAt(i) == s3.charAt(i + j)) dp[i][j] = dp[i-1][j];
        // if(s2.charAt(j) == s3.charAt(i + j)) dp[i][j] = dp[i][j-1];
        int n1 = s1.length();
        int n2 = s2.length();
        int n3 = s3.length();
        if (n1 + n2 != n3) {
            return false;
        }
        boolean[][] dp = new boolean[n1 + 1][n2 + 1];
        dp[0][0] = true;
        for (int i = 0; i <= n1; i++) {
            for (int j = 0; j <= n2; j++) {
                // i + j - 1
                if (i >= 1 && s1.charAt(i - 1) == s3.charAt(i + j - 1)) {
                    dp[i][j] |= dp[i - 1][j];
                }
                if (j >= 1 && s2.charAt(j - 1) == s3.charAt(i + j - 1)) {
                    dp[i][j] |= dp[i][j - 1];
                }
            }
        }

        return dp[n1][n2];

        // "ab"
        // "a"
        // "aab" "aba"
        /*
          a b
        a
         */

    }
}
