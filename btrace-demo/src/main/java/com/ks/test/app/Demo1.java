package com.ks.test.app;

public class Demo1 {

    public static void main(String[] args) throws Exception {

        for (int i = 0; i < 100; i++) {
            final int finalI = i;
            new Thread(() -> System.out.println("Thread start : " + finalI)).start();
            Thread.sleep(1000 * 10);
        }
    }

}
