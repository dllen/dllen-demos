## 测试方式

**解压 [btrace-v2.2.4-bin.tar.gz](https://github.com/btraceio/btrace/releases)**

```shell
mkdir btrace
tar -xvf btrace-v2.2.4-bin.tar.gz -C btrace
```

**运行测试代码**

运行 Demo1.java
使用 jps 获取 进程 ID

```shell
jps

# 输出
34208 Launcher
34209 Demo1

```

**运行 btrace 脚本**

```shell
./bin/btrace -p 10240 34209  samples/Memory.java
```

运行结果

```text
[main] INFO org.openjdk.btrace.client.Main - Attaching BTrace to PID: 34209
[main] INFO org.openjdk.btrace.client.Client - Successfully started BTrace probe: samples/Memory.java
Heap:

init = 268435456(262144K) used = 48369768(47236K) committed = 257425408(251392K) max = 3817865216(3728384K)

Non-Heap:

init = 2555904(2496K) used = 12995864(12691K) committed = 13828096(13504K) max = -1(-1K)

Heap:

init = 268435456(262144K) used = 49711968(48546K) committed = 257425408(251392K) max = 3817865216(3728384K)

Non-Heap:

init = 2555904(2496K) used = 12997944(12693K) committed = 13828096(13504K) max = -1(-1K)

Heap:

init = 268435456(262144K) used = 49711968(48546K) committed = 257425408(251392K) max = 3817865216(3728384K)

Non-Heap:

init = 2555904(2496K) used = 13001512(12696K) committed = 13828096(13504K) max = -1(-1K)

Heap:

init = 268435456(262144K) used = 49711968(48546K) committed = 257425408(251392K) max = 3817865216(3728384K)

Non-Heap:

init = 2555904(2496K) used = 13001640(12696K) committed = 13828096(13504K) max = -1(-1K)

Heap:

init = 268435456(262144K) used = 51054168(49857K) committed = 257425408(251392K) max = 3817865216(3728384K)
```
