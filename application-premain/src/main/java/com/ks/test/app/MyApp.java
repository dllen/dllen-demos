package com.ks.test.app;

import java.util.concurrent.TimeUnit;


public class MyApp {

    public static void main(String[] args) {
        while (true) {
            printHello();
        }
    }


    private static void printHello() {
        System.out.println("Hello World!");

        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
