package com.ks.test.app;

import javassist.ClassPool;
import javassist.bytecode.ClassFile;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.CodeIterator;
import javassist.bytecode.MethodInfo;
import javassist.bytecode.Mnemonic;

public class Demo2 {

    public static void main(String[] args) throws Exception {
        ClassPool classPool = ClassPool.getDefault();

        ClassFile classFile = classPool.get("com.ks.test.app.Point").getClassFile();

        MethodInfo methodInfo = classFile.getMethod("move");

        CodeAttribute codeAttribute = methodInfo.getCodeAttribute();

        CodeIterator codeIterator = codeAttribute.iterator();

        System.out.println("start print move byte code....");
        System.out.println();
        while (codeIterator.hasNext()) {
            int index = codeIterator.next();
            int op = codeIterator.byteAt(index);
            System.out.println(Mnemonic.OPCODE[op]);
        }
        System.out.println();
        System.out.println("end print move byte code....");
    }

}
