package com.ks.test.app;

import java.lang.reflect.Field;
import javassist.ClassPool;
import javassist.bytecode.AccessFlag;
import javassist.bytecode.ClassFile;
import javassist.bytecode.FieldInfo;

public class Demo1 {

    public static void main(String[] args) throws Exception {

        ClassFile cf = new ClassFile(false, "com.ks.test.app.Demo", null);

        cf.setInterfaces(new String[]{"java.io.Serializable"});

        FieldInfo fieldInfo = new FieldInfo(cf.getConstPool(), "id", "I");
        fieldInfo.setAccessFlags(AccessFlag.PUBLIC);
        cf.addField(fieldInfo);

        ClassPool classPool = ClassPool.getDefault();

        Field[] fields = classPool.makeClass(cf).toClass().getFields();

        //
        System.out.println(fields[0].getName());
    }

}
