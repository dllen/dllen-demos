package com.ks.test.app;

import java.lang.reflect.Field;
import javassist.ClassPool;
import javassist.bytecode.AccessFlag;
import javassist.bytecode.ClassFile;
import javassist.bytecode.FieldInfo;

public class Demo3 {

    public static void main(String[] args) throws Exception {
        ClassPool classPool = ClassPool.getDefault();

        ClassFile classFile = classPool.get("com.ks.test.app.Point").getClassFile();

        FieldInfo fieldInfo = new FieldInfo(classFile.getConstPool(), "id", "I");
        fieldInfo.setAccessFlags(AccessFlag.PUBLIC);

        classFile.addField(fieldInfo);

        // only get public fields
        Field[] fields = classPool.makeClass(classFile).toClass().getFields();

        for (Field field : fields) {
            System.out.println(field.getName());
        }
    }

}
