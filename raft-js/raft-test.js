const RaftNode = require('./raft-node');

// Create a cluster of 5 nodes
const nodes = [];
for (let i = 1; i <= 5; i++) {
  nodes.push(new RaftNode(i));
}

// Connect nodes to each other
nodes.forEach(node => {
  node.nodes = nodes;
});

// Wait for a leader to be elected
setTimeout(() => {
  const leader = nodes.find(node => node.state === 'leader');
  if (leader) {
    console.log(`Node ${leader.id} is the leader`);
    
    // Send a client request to the leader
    leader.clientRequest({ type: 'set', key: 'foo', value: 'bar' });
    
    // Check the logs after some time
    setTimeout(() => {
      nodes.forEach(node => {
        console.log(`Node ${node.id} log:`, node.log);
      });
    }, 500);
  } else {
    console.log('No leader elected yet');
  }
}, 1000);