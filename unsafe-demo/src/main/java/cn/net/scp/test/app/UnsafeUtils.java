package cn.net.scp.test.app;

import java.lang.reflect.Field;
import sun.misc.Unsafe;


// org.springframework.objenesis.instantiator.util.UnsafeUtils
public class UnsafeUtils {

    private static Unsafe unsafe;

    private UnsafeUtils() {
    }

    public static Unsafe getUnsafe() {
        return unsafe;
    }

    static {
        Field f = null;
        try {
            f = Unsafe.class.getDeclaredField("theUnsafe");
            f.setAccessible(true);
        } catch (NoSuchFieldException var3) {
        }

        if (f != null) {
            try {
                unsafe = (Unsafe) f.get((Object) null);
            } catch (IllegalAccessException var2) {
            }
        }


    }

}
