package cn.net.scp.test.app;


public class ParkUtils {

    public static void main(String[] args) throws InterruptedException {

    }


    public static void park() {
        UnsafeUtils.getUnsafe().park(false, 0L);
    }

    public static void unpark(Thread thread) {
        if (thread != null) {
            UnsafeUtils.getUnsafe().unpark(thread);
        }
    }
}
