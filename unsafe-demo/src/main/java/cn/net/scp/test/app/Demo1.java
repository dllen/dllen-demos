package cn.net.scp.test.app;

import java.lang.reflect.Field;

/**
 * 通过反射获取属性
 */
public class Demo1 {

    private String theUnsafe;

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        Demo1 demo1 = new Demo1();

        Field field = Demo1.class.getDeclaredField("theUnsafe");
        field.setAccessible(true);
        Object o = field.get(demo1);
        System.out.println("o : " + o);
    }

}
