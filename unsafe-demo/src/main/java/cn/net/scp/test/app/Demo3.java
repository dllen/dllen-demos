package cn.net.scp.test.app;

import java.io.IOException;

/**
 * 手动抛出异常
 */
public class Demo3 {

    public static void main(String[] args) {
        UnsafeUtils.getUnsafe().throwException(new IOException());
    }

}
