package cn.net.scp.test.app;


public class Demo2 {

    public static void main(String[] args) throws Exception {
        C1 c1 = (C1) UnsafeUtils.getUnsafe().allocateInstance(C1.class);

        System.out.println(c1.getA());
    }

}


class C1 {

    private long a;

    public C1() {
        this.a = 1;
    }

    public long getA() {
        return a;
    }
}
