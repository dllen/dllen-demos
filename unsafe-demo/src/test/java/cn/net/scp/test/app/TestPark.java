package cn.net.scp.test.app;


import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.LockSupport;
import org.junit.jupiter.api.Test;


public class TestPark {

    @Test
    void giveThreadWhenNotifyWithoutAcquiringMonitorThrowsException() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                synchronized (this) {
                    try {
                        this.wait();
                    } catch (InterruptedException e) {
                        // The thread was interrupted
                    }
                }
            }
        };

        /*
         Unlike wait() and notify(), park() and unpark(Thread) don’t require a monitor. Any code that can get a reference to the parked thread can unpark it. This might be useful in low-level code but can introduce additional complexity and hard-to-debug problems.
         Monitors are designed in Java so that a thread cannot use it if it hasn’t acquired it in the first place. This is done to prevent race conditions and simplify the synchronization process. Let’s try to notify a thread without acquiring it’s monitor:
         */
        assertThrows(IllegalMonitorStateException.class, () -> {
            thread.start();
            Thread.sleep(TimeUnit.SECONDS.toMillis(1));
            thread.notify();
            thread.join();
        });
        /*
        Trying to notify a thread without acquiring a monitor results in IllegalMonitorStateException. This mechanism enforces better coding standards and prevents possible hard-to-debug problems.
         */
    }


    @Test
    void giveThreadWhenUnparkWithoutAcquiringMonitor() {
        Thread thread = new Thread(LockSupport::park);
        assertTimeoutPreemptively(Duration.of(2, ChronoUnit.SECONDS), () -> {
            thread.start();
            LockSupport.unpark(thread);
        });
        /*
        We can control threads with little work. The only thing required is the reference to the thread. This provides us with more power over locking, but at the same time, it exposes us to many more problems.
        It’s clear why park() and unpark(Thread) might be helpful for low-level code, but we should avoid this in our usual application code because it might introduce too much complexity and unclear code.
         */
    }


    @Test
    void givenWaitingThreadWhenNotInterruptedShouldNotHaveInterruptedFlag() throws InterruptedException {

        Thread thread = new Thread() {
            @Override
            public void run() {
                synchronized (this) {
                    try {
                        this.wait();
                    } catch (InterruptedException e) {
                        // The thread was interrupted
                    }
                }
            }
        };

        thread.start();
        Thread.sleep(TimeUnit.SECONDS.toMillis(1));
        thread.interrupt();
        thread.join();
        assertFalse(thread.isInterrupted(), "The thread shouldn't have the interrupted flag");

        /*
        If we’re interrupting a thread from its waiting state, the wait() method would immediately throw an InterruptedException and clear the interrupted flag. That’s why the best practice is to use while loops checking the waiting conditions instead of the interrupted flag.
         */
    }


    @Test
    void givenParkedThreadWhenInterruptedShouldNotResetInterruptedFlag() throws InterruptedException {
        Thread thread = new Thread(LockSupport::park);
        thread.start();
        thread.interrupt();
        assertTrue(thread.isInterrupted(), "The thread should have the interrupted flag");
        thread.join();
    }

    private final Thread parkedThread = new Thread() {
        @Override
        public void run() {
            LockSupport.unpark(this);
            LockSupport.park();
        }
    };

    @Test
    void givenThreadWhenPreemptivePermitShouldNotPark()  {
        assertTimeoutPreemptively(Duration.of(1, ChronoUnit.SECONDS), () -> {
            parkedThread.start();
            parkedThread.join();
        });
    }

    private final Thread parkedThread2 = new Thread() {
        @Override
        public void run() {
            LockSupport.unpark(this);
            LockSupport.unpark(this);
            LockSupport.park();
            LockSupport.park();
        }
    };

    /*
        In this case, the thread would have only one permit, and the second call to the park() method would park the thread. This might produce some undesired behavior if not appropriately handled.
    */
    @Test
    void givenThreadWhenRepeatedPreemptivePermitShouldPark()  {
        Callable<Boolean> callable = () -> {
            parkedThread2.start();
            parkedThread2.join();
            return true;
        };

        boolean result = false;
        Future<Boolean> future = Executors.newSingleThreadExecutor().submit(callable);
        try {
            result = future.get(1, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            // Expected the thread to be parked
        }
        assertFalse(result, "The thread should be parked");
    }

}
